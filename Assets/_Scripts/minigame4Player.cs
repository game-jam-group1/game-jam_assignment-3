﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class minigame4Player : MonoBehaviour
{
    public float speed = 4f;
    public float movement = 0f;
    public Rigidbody2D rb;

    public static bool playerDied;
    public static bool gameWin = false;

    public Text health;
    public int healthNum = 10;

    // Update is called once per frame
    void Update()
    {
        

        movement = Input.GetAxis("Horizontal") * speed;

        if (GameObject.FindGameObjectsWithTag("Asteroid").Length <= 0)
        {
            gameWin = true;
            
        } 

        health.text = healthNum.ToString();

    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + new Vector2 (movement * Time.fixedDeltaTime, 0f));

       
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
      
        if (collision.collider.tag == "Asteroid")
        {
             healthNum -= 1;
             soundManager.instance.SoundOfAsteroid();

            if(healthNum <= 0)
            {
                playerDied = true;
                gameObject.SetActive(false);
            }
      
        }
    }
}
