﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour  
{
    public static soundManager instance;
    
    [SerializeField]
    private AudioSource jumpSound, hitSound, gameWinSound, asteroidHit, laserShot;



    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void SoundOfJump()
    {
        jumpSound.Play();
    }
    public void SoundOfHit()
    {
        hitSound.Play();
    }
    public void SoundOfWin()
    {
        gameWinSound.Play();
    }
    public void SoundOfAsteroid()
    {
        asteroidHit.Play();
    }
    public void LaserShot()
    {
        laserShot.Play();
    }
}
