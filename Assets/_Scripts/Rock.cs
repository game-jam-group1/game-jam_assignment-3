﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{

    public GameObject rock;

    //Destory based on what is touched
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
        else
        {
            StartCoroutine(deleteRock());
        }
    }

    IEnumerator deleteRock()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
