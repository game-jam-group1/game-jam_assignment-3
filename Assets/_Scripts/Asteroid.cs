﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public Vector2 startForce;

    public GameObject nextAsteroid;

    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb.AddForce(startForce, ForceMode2D.Impulse);
    }
    public void Split()
    {
        if (nextAsteroid != null)
        {
            GameObject asteroid1 = Instantiate(nextAsteroid, rb.position + Vector2.right / 2, Quaternion.identity);
            GameObject asteroid2 = Instantiate(nextAsteroid, rb.position + Vector2.left / 2, Quaternion.identity);

            asteroid1.GetComponent<Asteroid>().startForce = new Vector2(2f, 5f);
            asteroid2.GetComponent<Asteroid>().startForce = new Vector2(-2f, 5f);
        }

        Destroy(gameObject);
    }
}
