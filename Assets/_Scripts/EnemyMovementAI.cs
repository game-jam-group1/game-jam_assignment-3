﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

//Referenced this code for help with the movement of the enemy around the track: https://answers.unity.com/questions/1229374/changing-navmesh-target.html
public class EnemyMovementAI : MonoBehaviour
{

    public NavMeshAgent agent;

    public Transform node1;
    public Transform node2;
    public Transform node3;
    public Transform node4;
    public Transform node5;
    public Transform node6;
    public Transform node7;
    public Transform node8;

    private Transform[] destinations = new Transform[8];
    public int currentDestination = 0;

    public GameObject player;

    public static bool player1st;
    public static bool player2nd;

    public Text place;

    public static bool enemyWon;

    // Start is called before the first frame update
    void Start()
    {
        //Set each node in the array of destinations
        destinations[0] = node1;
        destinations[1] = node2;
        destinations[2] = node3;
        destinations[3] = node4;
        destinations[4] = node5;
        destinations[5] = node6;
        destinations[6] = node7;
        destinations[7] = node8;

        player = GameObject.Find("Stylized Astronaut");
        
    }

    // Update is called once per frame
    void Update()
    {
      if(UIManager.startMG2 == true)
        {
            run();

        } 
            
    }

    public void run()
    {
        if(currentDestination == 0)
        {
            agent.SetDestination(destinations[0].position);
        }
        //If not at the end of the array
        if (currentDestination < destinations.Length - 1)
        {
            //Check between distance of the enemy to the node
            if (Vector3.Distance(transform.position, destinations[currentDestination].position) < 3f)
            {
                //Make the enemy go to the next node on the track
                currentDestination++;
                agent.SetDestination(destinations[currentDestination].position);
            }

            //Display what place the player is depending on if the player is in front or behind the enemy
            if (Vector3.Distance(transform.position, destinations[currentDestination].position) < Vector3.Distance(player.transform.position, destinations[currentDestination].position))
            {
                place.text = "2nd";

                player2nd = true;
                player1st = false;
            }
            else
            {
                place.text = "1st";

                player1st = true;
                player2nd = false;
            }
         
        }

        //Turn enemy speed off after player won
        if(Player.playerWon == true)
        {
            agent.speed = 0;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Rock2")
        {
            StartCoroutine(knockDown());

        }

        if(collision.gameObject.name == "winBoundary")
        {
            
            enemyWon = true;
        }
    }

    IEnumerator knockDown()
    {
        agent.speed = 0;

        yield return new WaitForSeconds(3f);

        agent.speed = 3.5f;
    }
}
