﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// I watched this video on how to use the following code to implement the hearts UI: https://www.youtube.com/watch?v=3uyolYVsiWc
public class HealthSystem : MonoBehaviour
{
    public int health;
    private int numOfHearts = 3;

    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public Text numOfFriends;

    void Update()
    {
        health = GameManager.overallHealth;
       // Debug.Log(health);
        // Check to make sure that the health is not larger than the number of heart containers
       for(int i = 0; i < hearts.Length; i++)
        {
            if(health > numOfHearts)
            {
                health = numOfHearts;
            }

            //Make the heart sprite a full heart
            if(i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            //Male the heart sprite an empty heart
            else
            {
                hearts[i].sprite = emptyHeart;
            }
            //Show the heart
            if(i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            //Don't show the heart
            else
            {
                hearts[i].enabled = false;
            }
        }

        numOfFriends.text = GameManager.numOfFriends.ToString();
    }

}
