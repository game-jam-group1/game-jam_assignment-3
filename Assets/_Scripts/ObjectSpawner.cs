﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject rock;
    public GameObject gem;
    private float respawnTime;
    private float randomNum;

    void Start()
    {
        StartCoroutine(spawnObjects());
    }

    //Creates a random object based on the random number generated
    private void objectCreator()
    {
        randomNum = Random.Range(0, 3);

        if(UIManager.startMG1 == true) {
            if (randomNum == 0 || randomNum == 1)
            {
                GameObject rockSpawned = Instantiate(rock, transform.position, Quaternion.identity);
            }
            if (randomNum == 2)
            {
                GameObject gemSpawned = Instantiate(gem, transform.position, Quaternion.identity);
            }
        }
           
    }

    // Spawn the objects after a certain amount of time
    //Objects can randomly spawn between 1 and 15 seconds 
    IEnumerator spawnObjects()
    {
        respawnTime = Random.Range(1, 16);

        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            objectCreator();
        }


    }
}
