﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingObjectMovement : MonoBehaviour
{
    public GameObject rollingObject;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
   

    private void OnCollisionEnter(Collision collision)
    {
        //Make the objects roll far after fall from the mountain
        if(collision.gameObject.name == "Terrain_1_1" || collision.gameObject.name == "Terrain_1")
        {
            rb.velocity += new Vector3(-1, 0, 0) * 2;

        }

        //Destroy the object if it hits the back boundary or hits the player
        if(collision.gameObject.name == "boundary" || collision.gameObject.name == "boundary_right")
        {
            Destroy(rollingObject);
        }

        if(collision.gameObject.tag == "Player")
        {
            
            Destroy(rollingObject);
        }
    }
}
