﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerInteraction : MonoBehaviour
{
    private Rigidbody rb;
    public static bool playerDied;
    public static bool gameWin;
    private cameraFollow camera_Follow;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        camera_Follow = Camera.main.GetComponent<cameraFollow>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerDied)
        {
            if(rb.velocity.sqrMagnitude > 60)
            {
               // playerDied = true;
                camera_Follow.CanFollow = false;

                soundManager.instance.SoundOfHit();
                
            }
        }
    }

    private void OnTriggerEnter(Collider target)
    {
        if(target.tag == "Spike")
        {
            playerDied = true;
            camera_Follow.CanFollow = false;
            gameObject.SetActive(false);
            soundManager.instance.SoundOfHit();
           
        }
    }

    private void OnCollisionEnter(Collision target)
    {
        if(target.gameObject.tag == "EndPlatform")
        {
            gameWin = true;
            soundManager.instance.SoundOfWin();
        }

        if(target.gameObject.tag == "boundary")
        {
            playerDied = true;
        }
          
    }
  
}
