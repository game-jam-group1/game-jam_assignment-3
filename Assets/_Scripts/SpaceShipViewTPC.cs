﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipViewTPC : MonoBehaviour
{
    //The ship
    public GameObject target;

    //Determine how far the camera is from the ship
    private Vector3 offset;
    void Start()
    {
        //Calculate the inital distance of the camera and the player
        offset = target.transform.position - transform.position;
    }

    void LateUpdate()
    {
        //Camera will follow the player as the player moves forward and backwards and rotation
        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        transform.position = target.transform.position - (rotation * offset);
        transform.LookAt(target.transform);

    }
}
