﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public GameObject gameOver, restart;

    public GameObject resultsPanel;
    public Text results;

    public static int overallHealth = 3;
    public static int numOfFriends = 0;
    public static int minigame3Health = 1;

    public static bool miniGame1Complete = false;
    public static bool miniGame2Complete = false;
    public static bool miniGame3Complete = false;
    public static bool miniGame4Complete = false;

   

    public static bool bossComplete = false;

 


    private cameraFollow camera_Follow;


    //Create singleton
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    { 
        gameOver.gameObject.SetActive(false);
        restart.gameObject.SetActive(false);
       

    }


    
    void Update()
    {
       

        //Either show a win or lose panel depending on player score for minigame 1
        if (SceneController.getCurrentSceneName() == "Minigame1")
        {
            //Give Game over screen
            //Decrease overall game health by one
            //Reset minigame health and gems collected
            if (Player.minigameHealth == 0)
            {
                resultsPanel.SetActive(true);
                results.text = "Game Over :(";
                UIManager.startMG1 = false;
                overallHealth -= 1;
                Player.minigameHealth = 3;
                Player.gemsCollected = 0;

            }
            if (Player.gemsCollected == 6)
            {
                resultsPanel.SetActive(true);
                results.text = "You've collected all the gems! You win!";
                UIManager.startMG1 = false;
                miniGame1Complete = true;
                numOfFriends += 1;
                Player.gemsCollected = 0;
                

            }

        }

        if (SceneController.getCurrentSceneName() == "Minigame2")
        {
            //Give game won screen
            //Increase number of friends by one
            if (Player.playerWon == true)
            {
                resultsPanel.SetActive(true);
                results.text = "You finished in 1st place! You win!";
                UIManager.startMG2 = false;
                miniGame2Complete = true;
                numOfFriends += 1;
                Player.playerWon = false;
            }
            //Give game over screen
            //Decrease overall health by one
            if (EnemyMovementAI.enemyWon == true)
            {
                resultsPanel.SetActive(true);
                results.text = "Game Over :(";
                UIManager.startMG2 = false;
                overallHealth -= 1;
                EnemyMovementAI.enemyWon = false;
            }

        }

        if (SceneController.getCurrentSceneName() == "Minigame3")
        {
            // void OnCollisionEnter(Collision target)
            // {
            if (playerInteraction.playerDied == true)
            {
                resultsPanel.SetActive(true);
                results.text = "Game Over :(";
                //camera_Follow.CanFollow = false;
                //gameObject.SetActive(false);
                //soundManager.instance.SoundOfHit();
                UIManager.startMG3 = false;
                overallHealth -= 1;
                playerInteraction.playerDied = false;


            }
            else if (playerInteraction.gameWin == true)
            {
                resultsPanel.SetActive(true);
                results.text = "You've reached the top! You win!";
                UIManager.startMG3 = false;
                miniGame3Complete = true;
                numOfFriends += 1;
                playerInteraction.gameWin = false;
            }
        }
        
        
 
        if (SceneController.getCurrentSceneName() == "Minigame4")
        {
          
            if (minigame4Player.gameWin == true)
            {

                resultsPanel.SetActive(true);
                results.text = "You protected the galaxy from the asteroids! You win!";
                UIManager.startMG4 = false;
                miniGame4Complete = true;
                minigame4Player.gameWin = false;

            }
            

            if (minigame4Player.playerDied == true)
            {
                resultsPanel.SetActive(true);
                results.text = "Game Over :(";
                UIManager.startMG4 = false;
                overallHealth -= 1;
                minigame4Player.playerDied = false;
            }

        }

               
     

        if(SceneController.getCurrentSceneName() == "Boss")
        {
            if(BossBattleController.bossHealth <= 0)
            {
                resultsPanel.SetActive(true);
                results.text = "You defeated the Onkrid King and saved your best friend!";
                UIManager.startBoss = false;
                bossComplete = true;

            }
            if(BossBattleController.allDead == true)
            {
                resultsPanel.SetActive(true);
                results.text = "Game Over :(";
                UIManager.startBoss = false;
                
                
            }
        }

        // If the player presses escape exit the game
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }


    }

    //Adds a point to the friend number for minigame4 if the player wins
    public void addOneFriendPoint()
    {
        if(minigame4Player.gameWin == true)
        {
            numOfFriends += 1;
        }
        
    }

    
    

}
