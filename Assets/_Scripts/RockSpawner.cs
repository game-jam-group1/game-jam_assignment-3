﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSpawner : MonoBehaviour
{
    public GameObject rock;
    private float respawnTime;
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnRocks());
    }

    private void rockCreator()
    {
        GameObject rockSpawned = Instantiate(rock, transform.position, Quaternion.identity);
    }

    IEnumerator spawnRocks()
    {
        

        while (true)
        {
            yield return new WaitForSeconds(4f);
            rockCreator();
        }


    }
}
