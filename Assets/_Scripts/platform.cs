﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class platform : MonoBehaviour
{
    [SerializeField]
    private Transform[] spikes;

    private bool fallDown;


    // Start is called before the first frame update
    void Start()
    {
        ActivatePlatform();
     
    }

    void ActivateSpike()
    {
        int index = Random.Range(0, spikes.Length);

        spikes[index].gameObject.SetActive(true);

        spikes[index].DOLocalMoveY(0.7f, 1.3f).
        SetLoops(-1, LoopType.Yoyo).SetDelay(Random.Range(3f, 5f));
    }

    void ActivatePlatform()
    {
        int chance = Random.Range(0, 100);

        if(chance > 70)
        {
            int type = Random.Range(0, 8);

            if(type == 0) { ActivateSpike(); }
            else if (type == 1) {}
            else if (type == 2) { fallDown = true; }
            else if (type == 3) { ActivateSpike(); }
            else if (type == 4) {}
            else if (type == 5) {}
            else if (type == 6) { ActivateSpike(); }
            else if (type == 7) { fallDown = true; }
        }
    }

    void InvokeFalling()
    {
        gameObject.AddComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision target)
    {
        if(target.gameObject.tag == "Player")
        {
            if (fallDown)
            {
                fallDown = false;
                Invoke("InvokeFalling", 1f);
            }
        }
    }

}
