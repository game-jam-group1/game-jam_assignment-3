using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	private Animator anim;
	private CharacterController controller;
	private Transform playerDis;

	public float speed = 600.0f;
	public float turnSpeed = 400.0f;
	private Vector3 moveDirection = Vector3.zero;
	public float gravity = 20.0f;
    

	public AudioSource hit;
	public AudioSource gem;
	


	public static int minigameHealth = 3;
	public Text healthText;
	public static int gemsCollected = 0;
    public Text gemText;

	private float distance;
	public GameObject enemy;
	public static bool playerWon;

    //Player movement code came with the stylized astronaut prefab by Maksim Bugrimov 
	void Start ()
    {
	    controller = GetComponent <CharacterController>();
		anim = gameObject.GetComponentInChildren<Animator>();
		playerDis = GetComponent<Transform>();
		enemy = GameObject.Find("FreeLichPolyart");

		AudioSource[] clips = GetComponents<AudioSource>();
		hit = clips[0];
		gem = clips[1];
	}

	void Update ()
    {
	    if (Input.GetKey ("w"))
        {
		    anim.SetInteger ("AnimationPar", 1);
		}
        else
        {
			anim.SetInteger ("AnimationPar", 0);
		}

		if(controller.isGrounded)
        {
			moveDirection = transform.forward * Input.GetAxis("Vertical") * speed;
		}

		float turn = Input.GetAxis("Horizontal");
		transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);
		controller.Move(moveDirection * Time.deltaTime);
		moveDirection.y -= gravity * Time.deltaTime;

        //Update scores to the UI for minigame 1
        if(SceneController.getCurrentSceneName() == "Minigame1")
        {
			healthText.text = minigameHealth.ToString();
			gemText.text = gemsCollected.ToString();

            if(minigameHealth == 0)
            {
				Destroy(gameObject);
            }
		}
        //Turn the player off after the enemy won
        if(SceneController.getCurrentSceneName() == "Minigame2")
        {
            if(EnemyMovementAI.enemyWon == true)
            {
				speed = 0;
            }
        }
       
	}

    private void OnCollisionEnter(Collision collision)
    {
        //Decrease health if player hits a rock
        if(collision.gameObject.tag == "Rock")
        {

			minigameHealth -= 1;
			hit.Play();

		}

        //Increase the number of gems collected when a player touches a gem
        if(collision.gameObject.tag == "Prize")
        {
			gemsCollected += 1;
			gem.Play();
			

        }

        //Run speed boost method
        if(collision.gameObject.tag == "boost")
        {
			gem.Play();
			StartCoroutine(controlSpeed());
			Destroy(collision.gameObject);
        }

        //Run knock down method
        if(collision.gameObject.tag == "Rock2")
        {
			hit.Play();
			StartCoroutine(knockDown());
			

        }
        //Run decrease speed method
        if(collision.gameObject.tag == "plant")
        {
			
			StartCoroutine(decreaseSpeed());
        }

        //Tells the game the player reached the finish line first and has won the minigame
        if(collision.gameObject.name == "winBoundary")
        {
			playerWon = true;
			
        }
    }

    //Controls the amount of time the player goes fast from the speed boost
	IEnumerator controlSpeed()
	{

		speed = 7;
	
	    yield return new WaitForSeconds(5f);

		speed = 3.75f;

	}

    //Cause the player to become inmobile for a few seconds when hit with a rock
    IEnumerator knockDown()
    {
		speed = 0;

		yield return new WaitForSeconds(3f);

		speed = 3.75f;
	}

    //Cause the player's speed to decrease when running though plants
    IEnumerator decreaseSpeed()
    {
		speed = 1;

		yield return new WaitForSeconds(2f);

		speed = 3.75f;
	}

}
