﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static string getCurrentSceneName()
    {
        string nameOfCurrentScene = SceneManager.GetActiveScene().name;

        return nameOfCurrentScene;
    }
}
