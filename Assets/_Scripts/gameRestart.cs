﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameRestart : MonoBehaviour
{
    public void RestartGame()
    {
        Debug.Log("Restart");
        //SceneManager.LoadScene("DinoNuke");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
