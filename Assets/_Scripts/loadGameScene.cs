﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadGameScene : MonoBehaviour
{
    public void loadMiniGame()
    {
        if (SceneController.getCurrentSceneName() == "GemDialog")
        {
            SceneManager.LoadScene("Minigame1");
        }
        if (SceneController.getCurrentSceneName() == "RaceDialog")
        {
            SceneManager.LoadScene("Minigame2");
        }
        if (SceneController.getCurrentSceneName() == "MountainDialog")
        {
            SceneManager.LoadScene("Minigame3");
        }
        if (SceneController.getCurrentSceneName() == "Asteroid4Dialog")
        {
            SceneManager.LoadScene("Minigame4");
        }
        if (SceneController.getCurrentSceneName() == "BossDialog")
        {
            SceneManager.LoadScene("Boss");
        }
    }
}    