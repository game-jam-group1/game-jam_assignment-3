﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpin : MonoBehaviour
{
    public GameObject planet;

    // Update is called once per frame
    void Update()
    {
        //Controls the speed and direction of the planets rotations
        if(planet.name == "Planet 1")
        {
            planet.transform.Rotate(0, 0, -25 * Time.deltaTime);
        }
        if(planet.name == "Planet 2")
        {
            planet.transform.Rotate(0, 0, 10 * Time.deltaTime);
        }
        if (planet.name == "Planet 3")
        {
            planet.transform.Rotate(0, 0, 40 * Time.deltaTime);
        }
        if (planet.name == "Planet 4")
        {
            planet.transform.Rotate(0, 0, -30 * Time.deltaTime);
        }
        if (planet.name == "Planet 5")
        {
            planet.transform.Rotate(0, 0, 15 * Time.deltaTime);
        }
        if (planet.name == "Planet 6")
        {
            planet.transform.Rotate(0, 0, -20 * Time.deltaTime);
        }
    }
}
