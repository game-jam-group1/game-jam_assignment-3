﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerShipMovement : MonoBehaviour
{
    //Set the speeds for the player ship moving or turning
    private float movementSpeed = 70;
    private float turningSpeed = 60;

    private string miniGameToLoad = "";
    private string dialogScenceToLoad = "";

    public GameObject enterPlanetMenu;
    public GameObject enterOnkridMenu;
    public GameObject alreadyComplete;
    public GameObject incompleteMenu;

    public Text planetName;

    // Update is called once per frame
    void Update()
    {
        //Rotates the player when either the right or left arrow keys are pressed (Or A or D)
        float horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime;
        transform.Rotate(0, horizontal, 0);

        //Moves the player forward and backwards when the player presses either the up or down arrow (Or W or S)
        float vertical = Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime;
        transform.Translate(vertical, 0, 0);


        //Moves the player up and when the player holds down E
        if (Input.GetKey(KeyCode.E))
        {
            transform.Translate(0, 1, 0);
        }

        //Moves the player down and when the player holds down C
        if (Input.GetKey(KeyCode.C))
        {
            transform.Translate(0, -1, 0);
        }

        //Show the game over scene if the player's overall health is 0
        if (GameManager.overallHealth <= 0)
        {
            SceneManager.LoadScene("TotalGameOver");
        }

        //SHow the game win screen once the player defeats the final boss
        if(GameManager.bossComplete == true)
        {
            GameManager.bossComplete = false;
            SceneManager.LoadScene("TotalGameWin");
        }


    }



    public void OnCollisionEnter(Collision collision)
    {
        //Set the UI to ask the player if they would like to enter the planet
        if (collision.gameObject.name == "Planet 1")
        {
            if (GameManager.miniGame1Complete == false)
            {
                enterPlanetMenu.SetActive(true);
                planetName.text = "Bat Planet?";
                dialogScenceToLoad = "GemDialog";
                miniGameToLoad = "Minigame1";
            }
            else
            {
                alreadyComplete.SetActive(true);
            }
        }
        if (collision.gameObject.name == "Planet 2")
        {
            if(GameManager.miniGame2Complete == false)
            {
                enterPlanetMenu.SetActive(true);
                planetName.text = "Slime Planet?";
                dialogScenceToLoad = "RaceDialog";
                miniGameToLoad = "Minigame2";
            }
            else
            {
                alreadyComplete.SetActive(true);
            }

        }
        if (collision.gameObject.name == "Planet 3")
        {
            if(GameManager.miniGame3Complete == false)
            {
                enterPlanetMenu.SetActive(true);
                planetName.text = "Skeleton Planet?";
                dialogScenceToLoad = "MountainDialog";
                miniGameToLoad = "Minigame3";
            }
            else
            {
                alreadyComplete.SetActive(true);
            }
        }
        if (collision.gameObject.name == "Planet 4")
        {
            if(GameManager.miniGame4Complete == false)
            {
                enterPlanetMenu.SetActive(true);
                dialogScenceToLoad = "Asteroid4Dialog";
                miniGameToLoad = "Minigame4";
                planetName.text = "Dragon Planet?";
            }
            else
            {
                alreadyComplete.SetActive(true);
            }

        }
        if (collision.gameObject.name == "Planet 5")
        {

            if(GameManager.miniGame1Complete == true && GameManager.miniGame2Complete == true && GameManager.miniGame3Complete == true && GameManager.miniGame4Complete == true)
            {
                enterPlanetMenu.SetActive(true);
                planetName.text = "Onkrid Planet?";
                dialogScenceToLoad = "BossDialog";
                miniGameToLoad = "Boss";
            }
            else
            {
                incompleteMenu.SetActive(true);
            }
            
        }
        

    }

    //Load the scene of the planet the player flew to
    public void yes()
    {

        enterPlanetMenu.SetActive(false);

        if (miniGameToLoad == "Minigame1" && GameManager.miniGame1Complete == false)
        {
            SceneManager.LoadScene(dialogScenceToLoad);
        }
       

        if (miniGameToLoad == "Minigame2" && GameManager.miniGame2Complete == false)
        {
            SceneManager.LoadScene(dialogScenceToLoad);
        }
        

        if (miniGameToLoad == "Minigame3" && GameManager.miniGame3Complete == false)
        {
            SceneManager.LoadScene(dialogScenceToLoad);
        }
        

        if (miniGameToLoad == "Minigame4" && GameManager.miniGame4Complete == false)
        {
            SceneManager.LoadScene(dialogScenceToLoad);
        }
     

        if (miniGameToLoad == "Boss")
        {
            SceneManager.LoadScene(dialogScenceToLoad);
        }
           
        dialogScenceToLoad = ""; 
    }

    //Cancel and do nothing
    public void no()
    {
        enterPlanetMenu.SetActive(false);

    }

    public void goBack()
    {
        alreadyComplete.SetActive(false);
        incompleteMenu.SetActive(false);
    }
}


