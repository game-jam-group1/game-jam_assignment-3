﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControllerMainNav : MonoBehaviour
{
    private AudioSource music;
    // Start is called before the first frame update
    void Start()
    {
        music = GetComponent<AudioSource>();  
    }

    // Update is called once per frame
    void Update()
    {
        //Adjust volume based on number of friends so far
        if(GameManager.numOfFriends == 1)
        {
            music.volume = .05f;
        }
        if(GameManager.numOfFriends == 2)
        {
            music.volume = .2f;
        }
        if(GameManager.numOfFriends == 3)
        {
            music.volume = .4f;
        }
        if(GameManager.numOfFriends == 4)
        {
            music.volume = 1f;
        }
    }
}
