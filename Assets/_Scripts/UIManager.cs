﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public Text instructions;
    public static int healthNum;
    public static int gemNum;
    public Text healthNumText;
    public Text gemNumText;

    public static Text place;
    public static bool startMG1 = false;
    public static bool startMG2 = false;
    public static bool startMG3 = false;
    public static bool startMG4 = false;

    public static bool startBoss = false;



    // Start is called before the first frame update

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (SceneController.getCurrentSceneName() == "Minigame1")
        {
            instructions.text = "Collect your alien friend's gem collection! Be sure to collect all 6 gems! Avoid getting hit by rocks! \n \n Move: WASD Keys \n Collect: Touch";
        }
        if(SceneController.getCurrentSceneName() == "Minigame2")
        {
            instructions.text = "Race against the Onkrid and win the race! Make sure to dodge obstacles and collect stars for a speed boosts! \n \n Move Forward: W \n Move Backwards: S \n Turn: A and D \n Collect: Touch";        
        }

        if(SceneController.getCurrentSceneName() == "Minigame3")
        {
            instructions.text = "Reach the top of the mountain! Avoid getting hit by spike traps and be careful, blocks might fall down! \n \n Move: AW or Left and Up arrow Keys";
        }
        if (SceneController.getCurrentSceneName() == "Minigame4")
        {
            instructions.text = "Fly and shoot out barriers to break the asteroids! \n \n Move: AD keys or Left and Right arrow keys";
        }


        if (SceneController.getCurrentSceneName() == "Boss")
        {
            instructions.text = "Defeat the Onkrid King and save your best friend! You and your allies will take turns attacking, but be careful, King Onkrid is fighting back! If you and your allies are all down, you lose!";
        }



    }

    // Make to the popup go away and start the spawner
    public void startMiniGame1()
    {
        startPanel.SetActive(false);
        startMG1 = true;
    }
    public void startMiniGame2()
    {
        startPanel.SetActive(false);
        startMG2 = true;
    }
    public void startMiniGame3()
    {
        startPanel.SetActive(false);
        startMG3 = true;
    }
    public void startMiniGame4()
    {
        startPanel.SetActive(false);
        startMG4 = true;
    }


    public void startBossBattle()
    {
        startPanel.SetActive(false);
        startBoss = true;
    }

    public void backToMainNav()
    {
        SceneManager.LoadScene("MainNavigation");
    }

    public void quit()
    {
        Application.Quit();

    }

}
