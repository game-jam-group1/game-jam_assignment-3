﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBattleController : MonoBehaviour
{
    //Character panels
    public GameObject bat;
    public GameObject skeleton;
    public GameObject astronaut;
    public GameObject slime;
    public GameObject dragon;
    public GameObject boss;

    //Characters
    public GameObject batObject;
    public GameObject skeletonObject;
    public GameObject astronautObject;
    public GameObject slimeObject;
    public GameObject dragonObject;
    public GameObject bossObject;

    //Character text health
    public Text batHealthText;
    public Text skeletonHealthText;
    public Text astronautHealthText;
    public Text slimeHealthText;
    public Text dragonHealthText;
    public Text bossHealthText;

    //Description of boss attack and its target
    public Text bossDesc;
    public Text bossTarget;


    //Health of characters
    private int batHealth = 25;
    private int skeletonHealth = 25;
    private int astronautHealth = 25;
    private int slimeHealth = 25;
    private int dragonHealth = 25;
    public static int bossHealth = 100;

    //Check to see if turn should be skipped
    private bool skipBat;
    private bool skipSkeleton;
    private bool skipAstronaut;
    private bool skipSlime;
    private bool skipDragon;

    //Keep track of each characters turn
    private bool batsTurn;
    private bool skeletonsTurn;
    private bool astronautsTurn;
    private bool slimesTurn;
    private bool dragonsTurn;
    private bool bossTurn;

    //Keep track of who is knocked out
    public bool batDead;
    public bool skeletonDead;
    public bool astronautDead;
    public bool slimeDead;
    public bool dragonDead;
    public static bool allDead;

    //Skip panels
    public GameObject batSkipPanel;
    public GameObject skeletonSkipPanel;
    public GameObject astronautSkipPanel;
    public GameObject slimeSkipPanel;
    public GameObject dragonSkipPanel;

    //Character fainted text
    public Text batDesc;
    public Text skeletonDesc;
    public Text astronautDesc;
    public Text slimeDesc;
    public Text dragonDesc;

    //Character animators
    public Animator batAnimator;
    public Animator skeletonAnimator;
    public Animator slimeAnimator;
    public Animator dragonAnimator;
    public Animator bossAnimator;

    //Controls boss's moves and targets
    private int randomMove;
    private int randomAlly;

    // Start is called before the first frame update
    void Start()
    {
        
        //Start with bat and move to the right, boss goes last
        bat.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        batHealthText.text = batHealth.ToString();
        skeletonHealthText.text = skeletonHealth.ToString();
        astronautHealthText.text = astronautHealth.ToString();
        slimeHealthText.text = slimeHealth.ToString();
        dragonHealthText.text = dragonHealth.ToString();
        bossHealthText.text = bossHealth.ToString();

        //Trigger skip panel if character's special move was used last turn
        if(batsTurn == true && skipBat == true)
        {
            bat.SetActive(false);
            batSkipPanel.SetActive(true);
        }
        if (skeletonsTurn == true && skipSkeleton == true)
        {
            skeleton.SetActive(false);
            skeletonSkipPanel.SetActive(true);
        }
        if (astronautsTurn == true && skipAstronaut == true)
        {
            astronaut.SetActive(false);
            astronautSkipPanel.SetActive(true);
        }
        if (slimesTurn == true && skipSlime == true)
        {
            slime.SetActive(false);
            slimeSkipPanel.SetActive(true);
        }
        if (dragonsTurn == true && skipDragon == true)
        {
            dragon.SetActive(false);
            dragonSkipPanel.SetActive(true);
        }

        //Controls the possible moves and targets depending on the random number picked
        if(bossTurn == true)
        {
            boss.SetActive(true);

            randomMove = Random.Range(0, 3);
            randomAlly = Random.Range(0, 5);

            if(randomMove == 0)
            {
                if(randomAlly == 0)
                {
                    batHealth -= 5;
                    bossTarget.text = "Bat!";
                    batAnimator.SetBool("hit", true);
                }
                if(randomAlly == 1)
                {
                    skeletonHealth -= 5;
                    bossTarget.text = "Skeleton!";
                    skeletonAnimator.SetBool("hit", true);
                }
                if(randomAlly == 2)
                {
                    astronautHealth -= 5;
                    bossTarget.text = "Astronaut!";
                }
                if(randomAlly == 3)
                {
                    slimeHealth -= 5;
                    bossTarget.text = "Slime!";
                    slimeAnimator.SetBool("hit", true);
                }
                if(randomAlly == 4)
                {
                    dragonHealth -= 5;
                    bossTarget.text = "Dragon!";
                    dragonAnimator.SetBool("hit", true);
                }
                bossTurn = false;
                boss.SetActive(true);
                bossDesc.text = "The king used Magic Mischief (-5 health) against ";
                bossAnimator.SetBool("attack1", true);

                
            }
            if(randomMove == 1)
            {
                batHealth -= 2;
                skeletonHealth -= 2;
                astronautHealth -= 2;
                slimeHealth -= 2;
                dragonHealth -= 2;


                batAnimator.SetBool("hit", true);
                skeletonAnimator.SetBool("hit", true);
                slimeAnimator.SetBool("hit", true);
                dragonAnimator.SetBool("hit", true);
                bossAnimator.SetBool("attack2", true);

                bossTurn = false;
                boss.SetActive(true);
                bossDesc.text = "The king used Energy Explosion (-2 health) against ";
                bossTarget.text = "everyone!";

            }
            if(randomMove == 2)
            {
                if (bossHealth >= 90)
                {
                    bossHealth = 100;
                }
                else
                {
                    bossHealth += 10;
                }

                bossTurn = false;
                boss.SetActive(true);
                bossAnimator.SetBool("attack2", true);
                bossDesc.text = "The king healed himself! (+10 health)";
                bossTarget.text = "";
            }
        }

        //Destroy the character if their health is 0
        if(batHealth <= 0)
        {
            batDead = true;
            Destroy(batObject);
            
        }
        if (skeletonHealth <= 0)
        {
            skeletonDead = true;
            Destroy(skeletonObject);
            
        }
        if (astronautHealth <= 0)
        {
            astronautDead = true;
            Destroy(astronautObject);
            
        }
        if (slimeHealth <= 0)
        {
            slimeDead = true;
            Destroy(slimeObject);
            
        }
        if (dragonHealth <= 0)
        {
            dragonDead = true;
            Destroy(dragonObject);
            
        }

        //Signal to the user that the character is unable to battle if they were knocked out
        if(batsTurn == true && batDead == true)
        {
            batSkipPanel.SetActive(true);
            bat.SetActive(false);
            batDesc.text = "Bat has fainted and is unable to battle!";
        }
        if (skeletonsTurn == true && skeletonDead == true)
        {
            skeletonSkipPanel.SetActive(true);
            skeleton.SetActive(false);
            skeletonDesc.text = "Skeleton has fainted and is unable to battle!";
        }
        if (astronautsTurn == true && astronautDead == true)
        {
            astronautSkipPanel.SetActive(true);
            astronaut.SetActive(false);
            batDesc.text = "Astronaut has fainted and is unable to battle!";
        }
        if (slimesTurn == true && slimeDead == true)
        {
            slimeSkipPanel.SetActive(true);
            slime.SetActive(false);
            slimeDesc.text = "Slime has fainted and is unable to battle!";
        }
        if (dragonsTurn == true && dragonDead == true)
        {
            dragonSkipPanel.SetActive(true);
            dragon.SetActive(false);
            dragonDesc.text = "Dragon has fainted and is unable to battle!";
        }


        //Set to true if all five allies are knocked out
        if(batDead == true && skeletonDead == true && astronautDead == true && slimeDead == true && dragonDead == true)
        {
            allDead = true;
        }
    }



    //Bat's moves
    public void poisonSpray()
    {
        bossHealth -= 3;
        bat.SetActive(false);
        skeleton.SetActive(true);
        batsTurn = false;
        skeletonsTurn = true;
        batAnimator.SetBool("attack2", true);
    }

    public void friendlyFlutter()
    {
        if(skeletonHealth >= 20)
        {
            skeletonHealth = 25;
        }
        else
        {
            skeletonHealth += 5;
        }
        bat.SetActive(false);
        skeleton.SetActive(true);
        batsTurn = false;
        skeletonsTurn = true;
        batAnimator.SetBool("attack1", true);
    }

    public void fangsOfFury()
    {
        bossHealth -= 10;
        skipBat = true;
        bat.SetActive(false);
        skeleton.SetActive(true);
        batsTurn = false;
        skeletonsTurn = true;
        batAnimator.SetBool("attack1", true);
    }
    


    //Skeleton's moves
    public void boneBlitz()
    {
        bossHealth -= 5;
        skeleton.SetActive(false);
        astronaut.SetActive(true);
        skeletonsTurn = false;
        astronautsTurn = true;
        skeletonAnimator.SetBool("attack1", true);
    }

    public void healingTouch()
    {
        if (astronautHealth >= 20)
        {
            astronautHealth = 25;
        }
        else
        {
            astronautHealth += 5;
        }
        skeleton.SetActive(false);
        astronaut.SetActive(true);
        skeletonsTurn = false;
        astronautsTurn = true;
        skeletonAnimator.SetBool("attack1", true);
    }

    public void femurFatality()
    {
        bossHealth -= 15;
        skipSkeleton = true;
        skeleton.SetActive(false);
        astronaut.SetActive(true);
        skeletonsTurn = false;
        astronautsTurn = true;
        skeletonAnimator.SetBool("attack1", true);
    }


    //Astronaut's moves
    public void lunarLanding()
    {
        bossHealth -= 7;
        astronaut.SetActive(false);
        slime.SetActive(true);
        astronautsTurn = false;
        slimesTurn = true;
    }

    public void astronautIcecream()
    {
        if (slimeHealth >= 20)
        {
            slimeHealth = 25;
        }
        else
        {
            slimeHealth += 5;
        }
        astronaut.SetActive(false);
        slime.SetActive(true);
        astronautsTurn = false;
        slimesTurn = true;
    }

    public void totalEclipse()
    {
        if (batHealth >= 15)
        {
            batHealth = 25;
        }
        else
        {
            batHealth += 5;
        }

        if (skeletonHealth >= 15)
        {
            skeletonHealth = 25;
        }
        else
        {
            skeletonHealth += 5;
        }

        if (astronautHealth >= 15)
        {
            astronautHealth = 25;
        }
        else
        {
            astronautHealth += 5;
        }

        if (slimeHealth >= 15)
        {
            slimeHealth = 25;
        }
        else
        {
            slimeHealth += 5;
        }

        if (dragonHealth >= 15)
        {
            dragonHealth = 25;
        }
        else
        {
            dragonHealth += 5;
        }

        skipAstronaut = true;
        astronaut.SetActive(false);
        slime.SetActive(true);
        astronautsTurn = false;
        slimesTurn = true;
    }


    //Slime's moves
    public void gooeyPunch()
    {
        bossHealth -= 2;
        slime.SetActive(false);
        dragon.SetActive(true);
        slimesTurn = false;
        dragonsTurn = true;
        slimeAnimator.SetBool("attack1", true);
    }

    public void healingPotion()
    {
        if (dragonHealth >= 20)
        {
            dragonHealth = 25;
        }
        else
        {
            dragonHealth += 5;
        }
        slime.SetActive(false);
        dragon.SetActive(true);
        slimesTurn = false;
        dragonsTurn = true;
        slimeAnimator.SetBool("attack1", true);
    }

    public void slimeTime()
    {
        slimeHealth = 25;
        skipSlime = true;
        slime.SetActive(false);
        dragon.SetActive(true);
        slimesTurn = false;
        dragonsTurn = true;
        slimeAnimator.SetBool("attack1", true);
    }


    //Dragon's moves
    public void fireBreath()
    {
        bossHealth -= 10;
        dragon.SetActive(false);
        dragonsTurn = false;
        bossTurn = true;
        dragonAnimator.SetBool("attack1", true);
    }

    public void dragonScales()
    {
        if (batHealth >= 20)
        {
            batHealth = 25;
        }
        else
        {
            batHealth += 5;
        }
        dragon.SetActive(false);
        dragonsTurn = false;
        bossTurn = true;
        dragonAnimator.SetBool("attack1", true);
    }

    public void fireFinale()
    {
        bossHealth -= 20;
        skipDragon = true;
        dragon.SetActive(false);
        dragonsTurn = false;
        bossTurn = true;
        dragonAnimator.SetBool("attack2", true);
    }






    //Skips the character's turn if the special moved was used last turn
    public void skipBatTurn()
    {
        skipBat = false;
        batsTurn = false;
        skeletonsTurn = true;
        skeleton.SetActive(true);
        batSkipPanel.SetActive(false);

    }

    public void skipSkeletonTurn()
    {
        skipSkeleton = false;
        skeletonsTurn = false;
        astronautsTurn = true;
        astronaut.SetActive(true);
        skeletonSkipPanel.SetActive(false);
    }

    public void skipAstronautTurn()
    {
        skipAstronaut= false;
        astronautsTurn = false;
        slimesTurn = true;
        slime.SetActive(true);
        astronautSkipPanel.SetActive(false);
    }

    public void skipSlimeTurn()
    {
        skipSlime = false;
        slimesTurn = false;
        dragonsTurn = true;
        dragon.SetActive(true);
        slimeSkipPanel.SetActive(false);
    }

    public void skipDragonTurn()
    {
        skipDragon = false;
        dragonsTurn = false;
        bossTurn = true;
        dragonSkipPanel.SetActive(false);
    }

    public void bossTurnOver()
    {
        boss.SetActive(false);
        batsTurn = true;
        bat.SetActive(true);
    }

}
