﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainColl : MonoBehaviour
{
    void OnTriggerEnter2D (Collider2D collider)
    {
        Chain.isFired = false;

        if (collider.tag == "Asteroid")
        {
            collider.GetComponent<Asteroid>().Split();
        }
    }
}
